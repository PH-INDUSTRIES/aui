package com.example.anothertest;


import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SwitchPreferenceCompat;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends PreferenceFragmentCompat /**implements Preference.OnPreferenceChangeListener*/ {


    Boolean b = false;
    DialogInterface.OnClickListener positive = new DialogInterface.OnClickListener() {

        public void onClick(DialogInterface dialog, int id) {
            System.out.println("positive");
            b = true;
        }
    };
    DialogInterface.OnClickListener negative = new DialogInterface.OnClickListener() {

        public void onClick(DialogInterface dialog, int id) {
            System.out.println("negative");
            b = false;
        }

    };

Preference pref;



    @Override
    public void onCreatePreferences(Bundle savedInstanceState,
                                    String rootKey) {
        setPreferencesFromResource(R.xml.preferences, rootKey);


        pref = findPreference("camera");
        pref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener(){
            @Override
        public boolean onPreferenceChange(final Preference preference, Object newValue) {
            b = false;
            if(pref.getSharedPreferences().getBoolean("camera",true)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("blah...")
                        .setTitle("Title")
                        .setPositiveButton("proceed", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int id) {
                                System.out.println("positive");
                                Log.i("p", "positive");
                                b = true;
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int id) {
                                System.out.println("negative");
                                Log.i("n", "negative");
                                //pref.setChecked(false);
                                b = false;
                            }

                        })
                        .create();
                builder.show();
            }
            return true;
        }});



/**
 final String cameraString = "Wenn Sie die Kamera ausschalten, können Sie keine Bilder und Videos aufnehmen. Videotelefonie ist ebenfalls deaktiviert. Kamera schaltet sich nicht von alleine wieder an. Es werden keine Bilddaten mehr an Amazon gesendet.";

 Preference reset = findPreference("camera");
 reset.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
@Override public boolean onPreferenceClick(Preference preference) {

new AlertDialog.Builder(getActivity())
.setMessage(cameraString)
.setIcon(android.R.drawable.ic_dialog_alert)
.setPositiveButton("Fortfahren", new DialogInterface.OnClickListener() {

public void onClick(DialogInterface dialog, int whichButton) {
//MetaDataSQLiteHelper database = MetaDataSQLiteHelper.getInstance(getActivity());
//database.deleteAllMetaData();
//Toast.makeText(getActivity(), getString(R.string.delete_dialog_success), Toast.LENGTH_SHORT).show();
}
})
.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
@Override public void onClick(DialogInterface dialogInterface, int i) {
Boolean b = reset.getSharedPreferences().getBoolean("camera",true);

}
}).show();


return true;
}
});
 */

    }

    /**
     * @Override public boolean onPreferenceChanged(SharedPreferences sharedPreferences, String key) {
     * if (key.equals("this")||true) {
     * final SwitchPreferenceCompat sp = (SwitchPreferenceCompat) getPreferenceScreen().findPreference(key);
     * System.out.println("blubbb");
     * if (sp.isChecked()&&false) {
     * System.out.println("enabled");
     * } else {
     * AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
     * builder.setMessage("blah...")
     * .setTitle("Title")
     * .setPositiveButton("proceed", new DialogInterface.OnClickListener() {
     * <p>
     * public void onClick(DialogInterface dialog, int id) {
     * System.out.println("positive");
     * }
     * })
     * .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
     * <p>
     * public void onClick(DialogInterface dialog, int id) {
     * sp.setChecked(true);
     * System.out.println("cancled");
     * }
     * })
     * .create();
     * builder.show();
     * }
     * }else{
     * Log.i("not","code not used");}
     * }
     */









}



