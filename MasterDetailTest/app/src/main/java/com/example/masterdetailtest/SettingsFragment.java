package com.example.masterdetailtest;


import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.fragment.app.Fragment;
import androidx.preference.EditTextPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceCategory;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceScreen;
import androidx.preference.SwitchPreferenceCompat;

import com.example.masterdetailtest.customPreference.ver3_Preference;
import com.example.masterdetailtest.database.DatabaseAccess;
import com.example.masterdetailtest.dummy.DummyContent;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends PreferenceFragmentCompat /**implements Preference.OnPreferenceChangeListener*/ {


    public int index;

    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    /**
     * The dummy content this fragment is presenting.
     */
    private DummyContent.DummyItem mItem;


    private String[] details;
    private String[] summaries;
    private String[] sensors;
    private int nSensors;
    private int version;//0-2


    public SettingsFragment(){
        this.index = index;
    }







    @Override
    public void onCreatePreferences(Bundle savedInstanceState,
                                    String rootKey) {
        version = ((AUI) this.getActivity().getApplication()).getVersion()-1;


        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            mItem = DummyContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));

        }




        Context actContext = getActivity();




        PreferenceScreen ps = getPreferenceManager().createPreferenceScreen(actContext);
        setPreferenceScreen(ps);



        TypedValue themeTypedValue = new TypedValue();
        actContext.getTheme().resolveAttribute(R.attr.preferenceTheme, themeTypedValue, true);
        ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(actContext, themeTypedValue.resourceId);


        //setting up the database
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(actContext);
        databaseAccess.open();

        summaries = databaseAccess.getSummaries(mItem.name).split(";");
        details = databaseAccess.getDetails(mItem.name).split(";");
        sensors = databaseAccess.getSensors(mItem.name).split(";");
        databaseAccess.close();


        PreferenceCategory cat = new PreferenceCategory(contextThemeWrapper);
        cat.setTitle("Einstellung des Datenschutzes für " +mItem.name);
        getPreferenceScreen().addPreference(cat);



        //Setting the Preferences from database
        for (int i = 0; i < summaries.length ; i++) {
            final String sensor = sensors[i];

            if(version<2) {


                SwitchPreferenceCompat prefSwitch = new SwitchPreferenceCompat(contextThemeWrapper);
                prefSwitch.setKey(sensor);
                prefSwitch.setTitle(sensor);
                if(version==1){
                    prefSwitch.setSummary(summaries[i]);
                }
                getPreferenceScreen().addPreference(prefSwitch);
            }else{
                ver3_Preference pref = new ver3_Preference(contextThemeWrapper,mItem.number,i);
                pref.setKey(sensor);

                getPreferenceScreen().addPreference(pref);
            }



            if(version==2) {
                final String detail = details[i];
                final int finalI = i;
                final String titleStatus = "Zugriff auf "+sensors[i];

                ver3_Preference pref = (ver3_Preference)findPreference(sensor);

                pref.setText(sensors[i], summaries[i]);
                pref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(final Preference preference) {
                        if (/**pref.getSharedPreferences().getBoolean(sensor, true)*/true) {
                            final ver3_Preference pref = (ver3_Preference) preference;
                            //pref.setChecked(!pref.isChecked());
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            final AlertDialog dialog = builder.setMessage(detail)
                                    .setTitle(titleStatus)
                                    .setPositiveButton("Aktivieren", new DialogInterface.OnClickListener() {

                                        public void onClick(DialogInterface dialog, int id) {

                                            System.out.println("positive");
                                            Log.i("p", "positive");
                                            pref.setStatus(false);
                                            DummyContent.statuses[mItem.number][finalI] = false;
                                        }
                                    })
                                    .setNegativeButton("Deaktivieren", new DialogInterface.OnClickListener() {

                                        public void onClick(DialogInterface dialog, int id) {
                                            System.out.println("negative");
                                            Log.i("n", "negative");
                                            pref.setStatus(true);
                                            DummyContent.statuses[mItem.number][finalI] = true;
                                        }

                                    })
                                    .create();
                            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                @Override
                                public void onShow(DialogInterface dialogInterface) {
                                    dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.rgb(255,0,0));
                                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.rgb(0,153,51));
                                }
                            });
                            dialog.show();
                        }
                        return true;
                    }
                });
            }

        }

    }

}