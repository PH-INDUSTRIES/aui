package com.example.masterdetailtest.customPreference;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.preference.Preference;
import androidx.preference.PreferenceCategory;
import androidx.preference.PreferenceScreen;
import androidx.preference.PreferenceViewHolder;

import com.example.masterdetailtest.R;
import com.example.masterdetailtest.dummy.DummyContent;

public class ver3_Preference extends Preference {

    TextView detail;
    TextView status;


    SpannableString ssActive;
    SpannableString ssDeactive;
    SpannableString sString;

    int device;
    int sensor;


    public ver3_Preference(Context context,int device, int sensor) {
        super(context);
        this.sensor = sensor;
        this.device = device;
        setLayoutResource(R.layout.ver3_preference);
        String active = "AKTIVIERT";
        String deactive = "DEAKTIVIERT";
        ssActive = new SpannableString(active);
        ssDeactive = new SpannableString(deactive);
        ssActive.setSpan(new RelativeSizeSpan(1.25f), 0,active.length(), 0); // set size
        ssActive.setSpan(new ForegroundColorSpan(Color.rgb(0,162,20)), 0, active.length(), 0);// set color
        ssDeactive.setSpan(new RelativeSizeSpan(1.25f), 0,deactive.length(), 0); // set size
        ssDeactive.setSpan(new ForegroundColorSpan(Color.RED), 0, deactive.length(), 0);// set color
    }

    @Override
    public void onBindViewHolder(PreferenceViewHolder holder) {
        super.onBindViewHolder(holder);
        View view = holder.itemView;
        detail = (TextView) view.findViewById(R.id.sensorText);
        status = (TextView) view.findViewById(R.id.statusText);
        setStatus(DummyContent.statuses[device][sensor]);
        detail.setText(sString);
    }

    public void setStatus(boolean status){
        if(!status){
            this.status.setText(ssActive);
        }else{
            this.status.setText(ssDeactive);
        }
    }

    public void setText(String sensor, String summary){
        sString = new SpannableString(sensor+"\n"+summary);
        sString.setSpan(new RelativeSizeSpan(1.5f), 0,sensor.length(), 0); // set size
        sString.setSpan(new StyleSpan(Typeface.BOLD),0,sensor.length(),0);
        sString.setSpan(new ForegroundColorSpan(Color.rgb(100,100,100)),sensor.length(),sString.length(),0);

    }

}
