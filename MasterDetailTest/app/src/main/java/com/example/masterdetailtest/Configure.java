package com.example.masterdetailtest;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

public class Configure extends AppCompatActivity implements View.OnClickListener{

    Button btnStart;
    Spinner sp_zet;
    Spinner sp_ver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.configure);
        btnStart =  findViewById(R.id.button);
        btnStart.setOnClickListener(this);
        sp_ver = findViewById(R.id.spinner_version);
        sp_zet = findViewById(R.id.spinner_zettel);
        String[] items_ver = new String[]{"1", "2", "3"};
        String[] items_zet = new String[]{"1", "2", "3","4","5","6","7","8"};
        ArrayAdapter<String> adapter_ver = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items_ver);
        ArrayAdapter<String> adapter_zet = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items_zet);
        sp_ver.setAdapter(adapter_ver);
        sp_zet.setAdapter(adapter_zet);
    }


    @Override
    public void onClick(View view) {

        int version = Integer.parseInt(sp_ver.getSelectedItem().toString());
        int zettel = Integer.parseInt(sp_zet.getSelectedItem().toString());
        ((AUI) this.getApplication()).setVersion(version);
        ((AUI) this.getApplication()).setZettel(zettel);

        startActivity( new Intent(this, ItemListActivity.class));
        this.finish();
    }
}
