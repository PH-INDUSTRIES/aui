package com.example.masterdetailtest.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;


public class DatabaseAccess {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess instance;

    /**
     * Private constructor to aboid object creation from outside classes.
     *
     * @param context
     */
    private DatabaseAccess(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
    }

    /**
     * Return a singleton instance of DatabaseAccess.
     *
     * @param context the Context
     * @return the instance of DabaseAccess
     */
    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    /**
     * Open the database connection.
     */
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }

    /**
     * Close the database connection.
     */
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }
    

    public String getSummaries(String name) {
        Cursor cursor = null;
        String summaries = "";
        try {
            cursor = database.rawQuery("SELECT Summarys FROM AUI_DataTable WHERE Name=?", new String[]{name + ""});
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                summaries = cursor.getString(cursor.getColumnIndex("Summarys"));
            }
            return summaries;
        } finally {
            cursor.close();
        }
    }

    public String getSensors(String name) {
        Cursor cursor = null;
        String sensors = "";
        try {
            cursor = database.rawQuery("SELECT Sensoren FROM AUI_DataTable WHERE Name=?", new String[]{name + ""});
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                sensors = cursor.getString(cursor.getColumnIndex("Sensoren"));
            }
            return sensors;
        } finally {
            cursor.close();
        }
    }

    public int getSensorsCount(String name) {
        Cursor cursor = null;
        int sensCount = 0;
        try {
            cursor = database.rawQuery("SELECT nSensoren FROM AUI_DataTable WHERE Name=?", new String[]{name + ""});
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                sensCount = cursor.getInt(cursor.getColumnIndex("nSensoren"));
            }
            return sensCount;
        } finally {
            cursor.close();
        }
    }

    public ArrayList<String> getDevices(String zettel){
    Cursor cursor = null;
    ArrayList<String> devices = new ArrayList<String>();
    try{
        cursor = database.rawQuery("SELECT Name as name FROM AUI_DataTable WHERE Gruppe=?", new String[]{zettel});
        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
        if ((cursor != null) && (cursor.getCount() > 0))
            devices.add(cursor.getString(cursor.getColumnIndex("name")));
        cursor.moveToNext();
    }

    }finally {
        cursor.close();
    }
    return devices;
    }

    public String getDetails(String name) {
        Cursor cursor = null;
        String details = "";
        try {
            cursor = database.rawQuery("SELECT Details FROM AUI_DataTable WHERE Name=?", new String[]{name + ""});
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                details = cursor.getString(cursor.getColumnIndex("Details"));
            }
            return details;
        } finally {
            cursor.close();
        }
    }

}