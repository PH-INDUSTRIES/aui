package com.example.masterdetailtest.dummy;

import android.content.Context;
import android.widget.Toast;

import com.example.masterdetailtest.AUI;
import com.example.masterdetailtest.database.DatabaseAccess;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<DummyItem> ITEMS = new ArrayList<DummyItem>();

    public static boolean[][] statuses;

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, DummyItem> ITEM_MAP = new HashMap<String, DummyItem>();


    /**
     * Imported CSV List
     */
    public static List<List<String>> machine_list = new ArrayList<>();
    public static final String COMMA_DELIMITER = ";";


    private static final int COUNT = 26;




    static {

        Context context = AUI.getAppContext();
        String zettel = AUI.getZettel();

        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(context);
        databaseAccess.open();
        ArrayList<String> devices = databaseAccess.getDevices(zettel);
        statuses = new boolean[devices.size()][];
        for (int i=0; i<devices.size(); i++
             ) {
            int nSensors = databaseAccess.getSensorsCount(devices.get(i));
            statuses[i]=new boolean[nSensors];
            for (boolean s:statuses[i]
                 ) {
                s = true;
            }
        }
        databaseAccess.close();


        for (int i = 0; i < devices.size(); i++) {
            addItem(new DummyItem(Integer.toString(i+1),devices.get(i),i));
        }

    }

    private static void addItem(DummyItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    private static DummyItem createDummyItem(int position) {
        return new DummyItem("default","default",0);
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class DummyItem {
        public final String id;
        public final String name;
        public final int number;

        public DummyItem(String id, String name, int number) {
            this.id = id;
            this.name = name;
            this.number = number;
        }
    }
}
