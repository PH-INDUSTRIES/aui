package com.example.masterdetailtest;

import android.app.Application;
import android.content.Context;

public class AUI extends Application {

    private static Context context;
    private static int version;

    public static int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public static String getZettel() {
        return Integer.toString(zettel);
    }

    public void setZettel(int zettel) {
        this.zettel = zettel;
    }

    private static int zettel;

    private static boolean[][] statuses;

    public void onCreate() {
        super.onCreate();
        AUI.context = getApplicationContext();
    }

    public void instantiateStatuses(boolean[][] statuses){
        this.statuses = statuses;
        for (boolean[] s:statuses
             ) {
            for (boolean s2:s
                 ) {
                s2=true;
            }
        }
    }


    public static Context getAppContext() {
        return AUI.context;
    }
}
