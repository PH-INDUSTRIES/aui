package com.example.settingstest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity{

    Button btnSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        //btnSettings =  findViewById(R.id.btn_Settings);
        //btnSettings.setOnClickListener(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_frame, new MySettingsFragment())
                .commit();
    }

/**@Override
    public void onClick(View view) {
        startActivity( new Intent(this, SettingsActivity.class));
        this.finish();
    }*/
}
